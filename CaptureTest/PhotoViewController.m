//
//  PhotoViewController.m
//  CaptureTest
//
//  Created by zartarn on 16.03.15.
//  Copyright (c) 2015 Fingo LLC. All rights reserved.
//

#import "PhotoViewController.h"
#import <CoreImage/CoreImage.h>
#import <ImageIO/ImageIO.h>
#import <AssertMacros.h>
#import <AssetsLibrary/AssetsLibrary.h>

#define TOOLS_HEIGHT 50.f
#define PHOTO_BTN_HEIGHT 80.f

@interface PhotoViewController ()

@property (nonatomic, strong) AVCaptureSession *session;
@property (nonatomic, strong) AVCaptureStillImageOutput *stillImageOutput;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *previewLayer;

@end

@implementation PhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor darkGrayColor];
    
    // tools
    UIView *tools = [[UIView alloc] init];
    tools.frame = (CGRect){0.f, 0.f, self.view.bounds.size.width, TOOLS_HEIGHT};
    tools.backgroundColor = [UIColor colorWithWhite:0. alpha:0.5];
    [self.view addSubview:tools];
    
    // buttons
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeBtn setBackgroundImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    closeBtn.frame = (CGRect){5.f, (TOOLS_HEIGHT - 36)/2.f, 36.f, 36.f};
    [closeBtn addTarget:self action:@selector(_close:) forControlEvents:UIControlEventTouchUpInside];
    [tools addSubview:closeBtn];

    UIButton *photoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [photoButton setBackgroundImage:[UIImage imageNamed:@"icon_camera.png"] forState:UIControlStateNormal];
    CGRect rect = (CGRect){0.f, 0.f, PHOTO_BTN_HEIGHT, PHOTO_BTN_HEIGHT};
    rect.origin = (CGPoint){CGRectGetMidX(self.view.bounds) - PHOTO_BTN_HEIGHT/2.f,
                            (self.view.bounds.size.height + (self.view.bounds.size.width + TOOLS_HEIGHT) - PHOTO_BTN_HEIGHT)/2.f};
    photoButton.frame = rect;
    [photoButton addTarget:self action:@selector(_makePhoto:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:photoButton];
    
    // camera
    [self startCamera];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [self startCamera];
}

#pragma mark - Camera

- (void)startCamera {
    
    NSError *error = nil;
    
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    [session setSessionPreset:AVCaptureSessionPresetPhoto];
    self.session = session;
    
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];

    if (error) {
        NSLog(@"Error create input device");
        [self _close:nil];
    }
    
    if ( [session canAddInput:deviceInput] ){
        [session addInput:deviceInput];
    }

    self.stillImageOutput = [AVCaptureStillImageOutput new];
    
    [_stillImageOutput setOutputSettings:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCMPixelFormat_32BGRA]
                                                                     forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
    
    
    if ( [session canAddOutput:self.stillImageOutput] )
        [session addOutput:self.stillImageOutput];


    self.previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    self.previewLayer.backgroundColor = [[UIColor blackColor] CGColor];
    self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;

    CGRect previewRect = CGRectMake(0,
                                    0, // TOOLS_HEIGHT,
                                    self.view.bounds.size.width,
                                    self.view.bounds.size.width + TOOLS_HEIGHT);
    [self.previewLayer setFrame:previewRect];
    
    [self.view.layer insertSublayer:_previewLayer atIndex:0];
    
    if(!session.isRunning)
        [session startRunning];
}

#pragma mark - actions

- (void)_close:(id)sender {
    [_session stopRunning];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)_makePhoto:(id)sender {
//    AVCaptureConnection *videoConnection = nil;
//    for (AVCaptureConnection *connection in self.stillImageOutput.connections) {
//        for (AVCaptureInputPort *port in [connection inputPorts]) {
//            if ([[port mediaType] isEqual:AVMediaTypeVideo] ) {
//                videoConnection = connection;
//                break;
//            }
//        }
//        if (videoConnection) { break; }
//    }
    
    AVCaptureConnection *videoConnection =
    [self.stillImageOutput.connections objectAtIndex:0];
    if ([videoConnection isVideoOrientationSupported])
        [videoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
    
    CGSize deviceSize = self.view.bounds.size;
    
    [self.stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
        
        CVPixelBufferRef pixelBuffer = (CVPixelBufferRef)CMSampleBufferGetImageBuffer(imageSampleBuffer);
        CFDictionaryRef attachments = CMCopyDictionaryOfAttachments(kCFAllocatorDefault, imageSampleBuffer, kCMAttachmentMode_ShouldPropagate);
        if (attachments) {
            NSLog(@"attachements: %@", attachments);
        } else {
            NSLog(@"no attachments");
        }
        
        CIImage *ciImage = [CIImage imageWithCVPixelBuffer:pixelBuffer];
//        CIImage *ciImage = [[CIImage alloc] initWithCVPixelBuffer:pixelBuffer options:(__bridge NSDictionary *)attachments];
        if (attachments)
            CFRelease(attachments);
        
        NSDictionary *imageOptions = nil;
        NSNumber *orientation = (__bridge NSNumber *)(CMGetAttachment(imageSampleBuffer, kCGImagePropertyOrientation, NULL));
        if (orientation) {
            imageOptions = [NSDictionary dictionaryWithObject:orientation forKey:CIDetectorImageOrientation];
        }
        
//        CFDictionaryRef attachments1 = CMCopyDictionaryOfAttachments(kCFAllocatorDefault,
//                                                                     imageSampleBuffer,
//                                                                     kCMAttachmentMode_ShouldPropagate);
        
        size_t originalW = CVPixelBufferGetWidth(pixelBuffer);
        size_t originalH = CVPixelBufferGetHeight(pixelBuffer);
        CGFloat toolsOffset = originalH*TOOLS_HEIGHT/deviceSize.width;
        CGFloat offset = floorf((originalW - originalH*(deviceSize.width + TOOLS_HEIGHT)/deviceSize.width)*0.5) + toolsOffset ;
        
        CIContext *context = [CIContext contextWithOptions:imageOptions];
        CGImageRef myImage = [context
                              createCGImage:ciImage
                              fromRect:CGRectMake(0, 0,
                                                  CVPixelBufferGetWidth(pixelBuffer),
                                                  CVPixelBufferGetHeight(pixelBuffer))];
        
        CGRect cropRect = (CGRect){offset, 0, originalH, originalH};
        
        CGImageRef resImage = CGImageCreateWithImageInRect(myImage, cropRect);
        
        UIImage *image = [UIImage imageWithCGImage:resImage scale:1 orientation:UIImageOrientationRight];
        CGImageRelease(resImage);
        
        // to camera roll
        
//        CFDictionaryRef metaDict = CMCopyDictionaryOfAttachments(NULL, imageSampleBuffer, kCMAttachmentMode_ShouldPropagate);
//        CFMutableDictionaryRef mutable = CFDictionaryCreateMutableCopy(NULL, 0, metaDict);
//        
//        NSDictionary *metaDict = [NSDictionary
//                                  dictionaryWithObjectsAndKeys:
//                                  [NSNumber numberWithFloat:self.currentLocation.coordinate.latitude], kCGImagePropertyGPSLatitude,
//                                  @"N", kCGImagePropertyGPSLatitudeRef,
//                                  [NSNumber numberWithFloat:self.currentLocation.coordinate.longitude], kCGImagePropertyGPSLongitude,
//                                  @"E", kCGImagePropertyGPSLongitudeRef,
//                                  @"04:30:51.71", kCGImagePropertyGPSTimeStamp,
//                                  nil];
        
        
        ALAssetsLibrary *library = [ALAssetsLibrary new];
        
        [library writeImageToSavedPhotosAlbum:myImage orientation:ALAssetOrientationRight completionBlock:^(NSURL *assetURL, NSError *error) {
            NSLog(@"asset library save :: %@", error);
            CGImageRelease(myImage);
        }];
        
//        [library writeImageToSavedPhotosAlbum:myImage metadata:imageOptions completionBlock:^(NSURL *assetURL, NSError *error) {
//        }];
        
        
//        [self writeCGImageToCameraRoll:myImage withMetadata:(__bridge id)attachments1];
        
//        if (attachments)
//            CFRelease(attachments);
//        CGImageRelease(myImage);
        
//
//        CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(imageSampleBuffer);
        
//        CVPixelBufferRef croppedBuffer = NULL;
//        
//        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
//                                 [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
//                                 [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey, nil];
//        
//        CGImageRef croppedImage;
//        CGContextRef croppedContext;
//        size_t croppedWidth = width / 1.65;
//        size_t croppedHeight = height / 1.65;
        
//        NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
//        UIImage *image = [[UIImage alloc] initWithData:imageData];
//        
//        CGRect rect = (CGRect){0, (image.size.height - image.size.width)/2.f, image.size.width, image.size.width};
//        
//        UIImage *destImage = [self cropImage:image toRect:rect];
        [_delegate didCaptureImage:image];
        
        NSLog(@"w: %f", image.size.width);
        NSLog(@"h: %f", image.size.height);
        
        [self _close:nil];
    }];
}

#pragma mark - ViewController preffered

- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - helpers

// utility routine used after taking a still image to write the resulting image to the camera roll
- (BOOL)writeCGImageToCameraRoll:(CGImageRef)cgImage withMetadata:(NSDictionary *)metadata
{
    CFMutableDataRef destinationData = CFDataCreateMutable(kCFAllocatorDefault, 0);
    CGImageDestinationRef destination = CGImageDestinationCreateWithData(destinationData,
                                                                         CFSTR("public.jpeg"),
                                                                         1,
                                                                         NULL);
    BOOL success = (destination != NULL);
    require(success, bail);
    {
        const float JPEGCompQuality = 0.85f; // JPEGHigherQuality
        CFMutableDictionaryRef optionsDict = NULL;
        CFNumberRef qualityNum = NULL;
        
        qualityNum = CFNumberCreate(0, kCFNumberFloatType, &JPEGCompQuality);
        if ( qualityNum ) {
            optionsDict = CFDictionaryCreateMutable(0, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
            if ( optionsDict )
                CFDictionarySetValue(optionsDict, kCGImageDestinationLossyCompressionQuality, qualityNum);
            CFRelease( qualityNum );
        }
        
        CGImageDestinationAddImage( destination, cgImage, optionsDict );
        success = CGImageDestinationFinalize( destination );
        
        if ( optionsDict )
            CFRelease(optionsDict);
    }
bail:
    {
        if (destinationData)
            CFRelease(destinationData);
        if (destination)
            CFRelease(destination);
        return success;
    }
    
    require(success, bail1);
    {
        CFRetain(destinationData);
        ALAssetsLibrary *library = [ALAssetsLibrary new];
        [library writeImageDataToSavedPhotosAlbum:(__bridge id)destinationData metadata:metadata completionBlock:^(NSURL *assetURL, NSError *error) {
            if (destinationData)
                CFRelease(destinationData);
        }];
    }
bail1:
    {
        if (destinationData)
            CFRelease(destinationData);
        if (destination)
            CFRelease(destination);
        return success;
    }
}

@end
