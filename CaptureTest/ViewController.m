//
//  ViewController.m
//  CaptureTest
//
//  Created by zartarn on 16.03.15.
//  Copyright (c) 2015 Fingo LLC. All rights reserved.
//

#import "ViewController.h"
#import "PhotoViewController.h"

@interface ViewController() <PhotoSnapDelegate>

@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"Photo" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(_photo:) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = (CGRect){0, 0, 150, 50};
    btn.center = (CGPoint){CGRectGetMidX(self.view.bounds), 50};
    [self.view addSubview:btn];
    
    // image
    self.imageView = [[UIImageView alloc] initWithFrame:(CGRect){0.f, 0.f, 300.f, 300.f}];
    _imageView.backgroundColor = [UIColor lightGrayColor];
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    _imageView.center = self.view.center;
    [self.view addSubview:_imageView];
}

#pragma mark - actions

- (void)_photo:(id)sender {
    PhotoViewController *v = [PhotoViewController new];
    v.delegate = self;
    [self presentViewController:v animated:YES completion:nil];
}

#pragma mark - PhotoSnapDelegate

- (void)didCaptureImage:(UIImage *)image {
    self.imageView.image = image;
}


@end
