//
//  main.m
//  CaptureTest
//
//  Created by zartarn on 16.03.15.
//  Copyright (c) 2015 Fingo LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
