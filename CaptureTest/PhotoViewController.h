//
//  PhotoViewController.h
//  CaptureTest
//
//  Created by zartarn on 16.03.15.
//  Copyright (c) 2015 Fingo LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVCaptureOutput.h>

@protocol PhotoSnapDelegate;

@interface PhotoViewController : UIViewController
@property (nonatomic, assign) id<PhotoSnapDelegate> delegate;
@end

@protocol PhotoSnapDelegate <NSObject>
- (void)didCaptureImage:(UIImage *)image;
@end