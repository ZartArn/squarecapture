//
//  AppDelegate.h
//  CaptureTest
//
//  Created by zartarn on 16.03.15.
//  Copyright (c) 2015 Fingo LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

